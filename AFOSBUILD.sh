rm -rf /opt/ANDRAX/dhcpig

mkdir /opt/ANDRAX/dhcpig

cp -Rf pig.py /opt/ANDRAX/dhcpig/pig.py

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Copy PACKAGE... PASS!"
else
  # houston we have a problem
  exit 1
fi

install dhcpig.1 -m 644 /usr/local/man/man1

if [ $? -eq 0 ]
then
  # Result is OK! Just continue...
  echo "Install man... PASS!"
else
  # houston we have a problem
  exit 1
fi

mandb

cp -Rf andraxbin/* /opt/ANDRAX/bin
rm -rf andraxbin

chown -R andrax:andrax /opt/ANDRAX
chmod -R 755 /opt/ANDRAX
